﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Utils
{
    public class Helper
    {
        public string ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString("x2");
            return sbinary;
        }

        public string UnixTimeStampTotalSeconds()
        {
            DateTime nx = new DateTime(1970, 1, 1); // UNIX epoch date
            TimeSpan ts = DateTime.UtcNow - nx;
            return ((long)ts.TotalSeconds).ToString();
        }
    }
}
