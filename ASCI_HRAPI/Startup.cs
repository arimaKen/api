﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Repositories.Impl;
using ASCI_HRAPI.Services;
using ASCI_HRAPI.Services.Impl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace ASCI_HRAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            services.AddTransient<IUserRepository>(o => new UserRepository(Configuration));
            services.AddTransient<IAPILogRepository>(o => new APILogRepository(Configuration));
            services.AddTransient<ISettingRepository>(o => new SettingRepository(Configuration));
            services.AddTransient<IEmployeeRepository>(o => new EmployeeRepository(Configuration));
            services.AddTransient<IHistoryCutiRepository>(o => new HistoryCutiRepository(Configuration));
            services.AddTransient<IReimbursementRepository>(o => new ReimbursementRepository(Configuration));
            services.AddTransient<IMasterCompanyRepository>(o => new MasterCompanyRepository(Configuration));


            services.AddSingleton<ITokenService, TokenService>();
            services.AddSingleton<IFreedcampService, FreedcampService>();
            services.AddSingleton<IEmployeeService, EmployeeService>();
            services.AddSingleton<IHistoryCutiService, HistoryCutiService>();
            services.AddSingleton<IRembursementService, ReimbursementService>();
            services.AddSingleton<IMasterCompanyService, MasterCompanyService>();



            services.AddAuthentication(IISDefaults.AuthenticationScheme);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.Use(async (context, next) =>
                {
                    context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                    await next();
                });
            }

            app.UseDefaultFiles(new DefaultFilesOptions
            {
                DefaultFileNames = new List<string> { "startup.html" }
            });
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}
