﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;

namespace ASCI_HRAPI.Services
{
    public interface IMasterCompanyService
    {
        MasterCompany getDetail(int id);
        MasterCompany add(MasterCompany company);
        List<MasterCompany> getMaster();
    }
}
