﻿using ASCI_HRAPI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Services
{
    public interface IFreedcampService
    {
        Task<APIResult> get(FreedcampDTO dto);
    }
}
