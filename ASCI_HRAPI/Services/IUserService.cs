﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;

namespace MMS_API.Services
{
    public interface IUserService
    {
        User login(string user_id, string pass);
        User login(string username, string pass, string device);
        bool logout(string user_id);

        List<User> search(List<string[]> param = null);

        User get(int id);
        User getByUsername(string username);
		User getByUserName(string userName, string devID);

		User add(User user);
        bool update(User user);
        bool del(int id);
    }
}
