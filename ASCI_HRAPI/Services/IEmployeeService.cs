﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;

namespace ASCI_HRAPI.Services
{
   public interface IEmployeeService
    {
        APIResultEmployee getByID(int id);
        APIResultEmployee login(String username, String password, string company);
    }
}
