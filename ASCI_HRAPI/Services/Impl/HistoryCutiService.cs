﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Entities;

namespace ASCI_HRAPI.Services.Impl
{
    public class HistoryCutiService : IHistoryCutiService
    {
        protected IHistoryCutiRepository _hstrCutiRepo;
        protected ISettingRepository _settingRepository;

        public HistoryCutiService(IHistoryCutiRepository hstrCutiRepo, ISettingRepository settingRepo)
        {
            _hstrCutiRepo = hstrCutiRepo;
            _settingRepository = settingRepo;
        }
        public APIResultHistoryCuti getIdCuti(int id)
        {
            APIResultHistoryCuti result = new APIResultHistoryCuti();
            HistoryCuti hist = new HistoryCuti();
            hist = _hstrCutiRepo.getIdCuti(id);
            if (result != null)
            foreach(var his in result.Cuti)
            {
                HistoryCutiDTO cuti = new HistoryCutiDTO();
                cuti.id_cuti = his.id_cuti;
                cuti.id_History = his.id_History;
                cuti.employee_name = his.employee_name;
                cuti.type_cuti = his.type_cuti;
                cuti.date_start = his.date_start;
                cuti.date_end = his.date_end;
                cuti.date_assign= his.date_assign;
                cuti.note = his.note;
                cuti.lampiran = his.lampiran;
                cuti.verifikasi = his.verifikasi;
                result.Cuti.Add(cuti);
            }
            return result;
        }
    }
}
