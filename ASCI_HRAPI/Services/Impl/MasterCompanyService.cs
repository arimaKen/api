﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.Repositories;

namespace ASCI_HRAPI.Services.Impl
{
    public class MasterCompanyService : IMasterCompanyService
    {
        protected IMasterCompanyRepository _compRepo;
        protected ISettingRepository _settingRepo;
        public MasterCompanyService(IMasterCompanyRepository compRepo, ISettingRepository settingRepo)
        {
            _compRepo = compRepo;
            _settingRepo = settingRepo;
        }

        public MasterCompany add(MasterCompany company)
        {
            MasterCompany mCompany = new MasterCompany();
            mCompany = _compRepo.add(company);
            return mCompany;
        }

        public MasterCompany getDetail(int id)
        {
          MasterCompany masterCompany = new MasterCompany();
            masterCompany = _compRepo.getDetails(id);

            return masterCompany;
        }

        public List<MasterCompany> getMaster()
        {
            List<MasterCompany> resMaster = new List<MasterCompany>();
            resMaster = _compRepo.getMaster();
            return resMaster;
            
        }
    }
}
