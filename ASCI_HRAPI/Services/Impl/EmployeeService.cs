﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Entities;

namespace ASCI_HRAPI.Services.Impl
{
    public class EmployeeService : IEmployeeService 
    {
        protected IEmployeeRepository _emplRepo;
        protected ISettingRepository _settingRepo;
        public EmployeeService(IEmployeeRepository emplRepo, ISettingRepository settingRepo)
        {
            _emplRepo = emplRepo;
            _settingRepo = settingRepo;
        }
        public APIResultEmployee getByID(int id)
        {
            APIResultEmployee result = new APIResultEmployee();
            result.Cuti = new List<CutiDTO>();
            Employee empl = new Employee();

            empl = _emplRepo.getByID(id);
            result.idKaryawan = empl.id_employee;
            result.username = empl.username;
            result.position = empl.posisi;
            result.fullname = empl.nama_depan + " " + empl.nama_belakang;
            result.email = empl.email;
            result.company = empl.company;
            foreach (var cut in empl.emplCuti) {
                CutiDTO cuti = new CutiDTO();
                cuti.id_cuti = cut.id_cuti;
                cuti.id_employee = cut.id_employee;
                cuti.bulan = cut.bulan;
                cuti.jatah = cut.jatah;
                cuti.sisa = cut.sisa;
                result.Cuti.Add(cuti);
                result.Cuti.ElementAt(0).historyCuti = new List<HistoryCutiDTO>();

                foreach (var hist in empl.emplCuti.ElementAt(0).hstrCuti)
                {
                    HistoryCutiDTO history = new HistoryCutiDTO();
                    history.id_cuti = hist.id_cuti;
                    history.id_History = hist.id_History;
                    history.employee_name = hist.employee_name;
                    history.date_start = hist.date_start;
                    history.date_end = hist.date_end;
                    history.date_assign = hist.date_assign;
                    history.type_cuti = hist.type_cuti;
                    history.note = hist.note;
                    history.lampiran = hist.lampiran;
                    history.verifikasi = hist.verifikasi;
                    result.Cuti.ElementAt(0).historyCuti.Add(history);
                }
            }
            return result;
        }

        public APIResultEmployee login(string username, string password, string company)
        {
            APIResultEmployee result = new APIResultEmployee();
            result.Cuti = new List<CutiDTO>();
            Employee empl = new Employee();

            empl = _emplRepo.login(username,password, company);
            result.idKaryawan = empl.id_employee;
            result.username = empl.username;
            result.position = empl.posisi;
            result.fullname = empl.nama_depan + " " + empl.nama_belakang;
            result.email = empl.email;
            result.company = empl.company;
            return result;
        }
    }
}

