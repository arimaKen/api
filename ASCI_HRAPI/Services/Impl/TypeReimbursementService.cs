﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.Services;

namespace ASCI_HRAPI.Services.Impl
{
    public class TypeReimbursementService : ITypeReimbursementService
    {
        protected ITypeReimbursementRepository _reimRepo;
        protected ISettingRepository _settingRepo;

        public TypeReimbursementService(ITypeReimbursementRepository reimRepo, ISettingRepository settingRepo)
        {
            _reimRepo = reimRepo;
            _settingRepo = settingRepo;
        }

        public List<TypeReimbursementDTO> getType()
        {
            List<TypeReimbursementDTO> reimbursements = new List<TypeReimbursementDTO>();
            reimbursements = _reimRepo.GetTypeReimbursements();

            return reimbursements;
        }
    }
}
