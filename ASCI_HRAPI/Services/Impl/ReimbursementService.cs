﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.Services;
using MMS_API.Repositories.Exceptions;

namespace ASCI_HRAPI.Services.Impl
{
    public class ReimbursementService : IRembursementService
    {
        protected IReimbursementRepository _reimRepo;
        protected ISettingRepository _settingRepo;

        public ReimbursementService(IReimbursementRepository reimRepo, ISettingRepository settingRepo)
        {
            _reimRepo = reimRepo;
            _settingRepo = settingRepo;
        }

        public List<APIResultReimbursement> getByIDEmployee(int id)
        {
           List<APIResultReimbursement> resultReimbursement = new List<APIResultReimbursement>();
            List<Reimbursement> reimbursements = new List<Reimbursement>();
            reimbursements = _reimRepo.getByIDEmployee(id);

            foreach(Reimbursement result in reimbursements)
            {
                APIResultReimbursement data = new APIResultReimbursement();
                data.id_reim = result.id_reim;
                data.id_employee =Convert.ToInt32(result.id_employee);
                data.nominal = result.nominal;
                data.tanggal = result.tanggal;
                data.tipe = result.tipe;
                data.catatan = result.catatan;
                data.pathLocal = result.pathLocal;
                data.photo = result.photo;
                data.status = result.status;
                resultReimbursement.Add(data);

            }

            return resultReimbursement;
        }

        public void add(Reimbursement Reimbursment)
        {
            try
            {
                _reimRepo.add(Reimbursment);
            }
            catch (DataExistsException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<TypeReimbursementDTO> getType()
        {
            List<TypeReimbursementDTO> reimbursements = new List<TypeReimbursementDTO>();
            reimbursements = _reimRepo.GetTypeReimbursements();

            return reimbursements;
        }

        HFormReim IRembursementService.addForm(HFormReim hForm)
        {
            try
            {
                _reimRepo.addFrom(hForm);
            }
            catch (DataExistsException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
            return hForm;
        }

        public Reimbursement getReimId(string id)
        {
          Reimbursement reimbursements = _reimRepo.getIdReim(id);

            return reimbursements;
        }

        public List<HFormReim> getForm(int id)
        {
            List<HFormReim> hFormReims = new List<HFormReim>();
            hFormReims = _reimRepo.getForm(id);
            return hFormReims;
        }

        public HFormReim getFormDetil(int id)
        {
           HFormReim hFormReims = new HFormReim();
            hFormReims = _reimRepo.getFormDetail(id);
            return hFormReims;
        }
    }
}
