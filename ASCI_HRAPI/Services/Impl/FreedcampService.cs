﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Utils;

namespace ASCI_HRAPI.Services.Impl
{
    public class FreedcampService : IFreedcampService
    {
        protected ISettingRepository _settingService;
        protected IUserRepository _userRepo;
        protected IAPILogRepository _apiLog;
        private static readonly Encoding encoding = Encoding.UTF8;
        protected HttpClient http = new HttpClient();

        public FreedcampService(ISettingRepository settingService, IUserRepository userRepo, IAPILogRepository apiLog)
        {
            _settingService = settingService;
            _userRepo = userRepo;
            _apiLog = apiLog;
        }

        public async Task<APIResult> get(FreedcampDTO dto)
        {
            APIResult result = new APIResult();
            Helper help = new Helper();
            AES aES = new AES();

            var encKey = _settingService.getByName("encrypt.key");

            var encPass = aES.Encrypt(encKey.setting_value, dto.pass);
            User user = _userRepo.Get(dto.user, encPass);

            var key_public = user.api_key;
            string key_private = user.secret_api_key;
            var ts = help.UnixTimeStampTotalSeconds();
            byte[] keyByte = encoding.GetBytes(key_private);

            try
            {
                using (var hmacsha1 = new HMACSHA1(keyByte))
                {
                    hmacsha1.ComputeHash(encoding.GetBytes(string.Concat(key_public, ts)));
                    var hashstr = help.ByteToString(hmacsha1.Hash);

                    var uril = "https://freedcamp.com/api/v1/"+  dto.url + "api_key=" + key_public + " &timestamp=" + ts + "&hash=" + hashstr;

                    var response = await http.GetAsync(uril);

                    if (response.IsSuccessStatusCode)
                    {
                        string data = await response.Content.ReadAsStringAsync();

                        result.error = "";
                        result.data_status = data;
                    }
                    else
                    {
                        //result.error = data;
                    }
                }
            }
            catch (Exception e)
            {
                result.error = e.Message;
            }

            return result;
        }
    }
}
