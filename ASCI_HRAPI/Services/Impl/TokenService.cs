﻿using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Utils;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using MMS_API.Services.Exceptions;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace ASCI_HRAPI.Services.Impl
{
    public class TokenService : ITokenService
    {
        protected IUserRepository _userRepository;
        protected ISettingRepository _settingRepository;

        protected IJwtEncoder _encoder;
        protected IJwtDecoder _decoder;



        public TokenService(IUserRepository userRepository, ISettingRepository settingRepository)
        {
            _userRepository = userRepository;
            _settingRepository = settingRepository;

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);

            _encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            _decoder = new JwtDecoder(serializer, validator, urlEncoder);

        }

        protected double toUnixTime(DateTime time)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = time.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        public static string EncodePassword(string pass, string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(pass);
            byte[] src = Encoding.Unicode.GetBytes(salt);
            byte[] dst = new byte[src.Length + bytes.Length];
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
            HashAlgorithm algorithm = SHA1.Create();
            byte[] inArray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inArray);
        }

        public AccessToken validate(string token)
        {
            AccessToken result = null;

            if (token != null)
            {
                //Bearer < token >
                //01234567
                if (token.StartsWith("Bearer ") || token.StartsWith("bearer "))
                    token = token.Substring(7);

                try
                {
                    string secret = _settingRepository.getValueByName("api.token.secret", "asci_secret_key_8899");

                    var payload = _decoder.DecodeToObject<IDictionary<string, object>>(token, secret, true);

                    result = new AccessToken();
                    result.name = payload["name"] == null ? "" : (payload["name"]).ToString();
                    result.role = payload["role"] == null ? "" : (payload["role"]).ToString();
                }
                catch (JWT.TokenExpiredException)
                {
                    throw new MMS_API.Services.Exceptions.TokenExpiredException();
                }
                catch (SignatureVerificationException)
                {
                    throw new TokenUnauthorizeException();
                }
            }
            else
                throw new TokenUnauthorizeException();

            return result;
        }

        public string login(string userName, string pass)
        {
            string result = null;
            string key = _settingRepository.getByName("encrypt.key").setting_value;

            User user = _userRepository.GetByUserName(userName);

            if ((user != null) && user.user_active.Equals("Y") && user.user_deleted.Equals("N") && user.usergroup_id.Equals(1))
            {
                AES aES = new AES();
                string encodedPassword = aES.Encrypt(key, pass);

                if (user.password.Equals(encodedPassword))
                {
                    int lifetime = int.Parse(_settingRepository.getValueByName("api.token.lifetime", "60"));
                    string secret = _settingRepository.getValueByName("api.token.secret", "asci_secret_key_8899");

                    var payload = new Dictionary<string, object>
                    {
                        { "name", userName },
                        { "role",  user.usergroup_id },
                        { "exp",  toUnixTime(System.DateTime.Now.AddMinutes(lifetime)) },
                        { "iat",  toUnixTime(System.DateTime.Now) }
                    };

                    result = _encoder.Encode(payload, secret);
                }
                else
                    throw new TokenWrongPasswordException();
            }
            else
                throw new TokenUserNotFoundException();

            return result;
        }
    }
}
