﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.DTO;

namespace ASCI_HRAPI.Services
{
   public interface IRembursementService
    {
       List<APIResultReimbursement> getByIDEmployee(int id);
        Reimbursement getReimId(string id);
       List<HFormReim> getForm(int id);
       HFormReim getFormDetil(int id);
        void add(Reimbursement Reimbursment);
        List<TypeReimbursementDTO> getType();
        HFormReim addForm(HFormReim hForm);
    }
}
