﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Entities;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class ReimbursementRepository : BaseRepository, IReimbursementRepository
    {
        public ReimbursementRepository(IConfiguration config) : base(config)
        {
        }

        public Reimbursement add(Reimbursement Reimbursment)
        {
            using (var conn = getConn())
            {
                Reimbursement objSd = new Reimbursement();
                Reimbursement result = null;


                conn.Open();

                using (var tran = conn.BeginTransaction())
                {
                    if (Reimbursment.id_reim > 0) conn.Update<Reimbursement>(Reimbursment, tran);
                    else conn.Insert<Reimbursement>(Reimbursment, tran);

                    tran.Commit();
                    result = Reimbursment;

                }
                return result;
            }
        }

        public HFormReim addFrom(HFormReim hFormReim)
        {
            using (var conn = getConn())
            {
                HFormReim newHReim = new HFormReim();
                List<DFormReim> listDFromReim = new List<DFormReim>();
                HFormReim result = null;

                conn.Open();

                using (var tran = conn.BeginTransaction())
                {

                    newHReim.id_employee = hFormReim.id_employee;
                    newHReim.nominal = hFormReim.nominal;
                    newHReim.subject = hFormReim.subject;
                    newHReim.date_assign = hFormReim.date_assign;
                    newHReim.status = "P";
                    conn.Insert<HFormReim>(newHReim, tran);
                    result = newHReim;

                    if (result != null)
                    {
                        foreach (Reimbursement dFormReim in hFormReim.details)
                        {
                            
                            Reimbursement existingDFromReim = getIdReim(dFormReim.id_reim.ToString());
                            existingDFromReim.id_form =result.id_form;
                            existingDFromReim.status = "NC";
                            conn.Update<Reimbursement>(existingDFromReim, tran);
                        }
                    }
                    else throw new Exception();
                    tran.Commit();
                }
                return result;
            }
        }

        public Reimbursement getIdReim(string id)
        {
            Reimbursement result = new Reimbursement();
            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"SELECT * FROM [dbo].[ASCI_REIM] WHERE id_reim = @id";

                result = conn.Query<Reimbursement>(sql, new { id = id }).FirstOrDefault();
            };

            return result;
        }

        public List<Reimbursement> getByIDEmployee(int id)
        {
            List<Reimbursement> result = null;
            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"SELECT [id_reim],[id_employee],[nominal],[tanggal],[tipe],[catatan],[pathLocal],[status] FROM [dbo].[ASCI_REIM] WHERE id_employee = @id";

                result = conn.Query<Reimbursement>(sql, new { id }).ToList();
            }
            return result;
        }

        public List<TypeReimbursementDTO> GetTypeReimbursements()
        {
            List<TypeReimbursementDTO> result = null;

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @" SELECT *
  FROM [ASCI_HR].[dbo].[ASCI_TYPE_REIM]";

                result = conn.Query<TypeReimbursementDTO>(sql).ToList();
            }
            return result;
        }

        public List<HFormReim> getForm(int id)
        {
            List<HFormReim> result = null;

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @" SELECT * FROM [ASCI_HR].[dbo].[ASCI_HFORM] where id_employee = @id";

                result = conn.Query<HFormReim>(sql,new { id = id }).ToList();
            }
            return result;
        }

        public HFormReim getFormDetail(int id)
        {
            HFormReim result = null;
            List<Reimbursement> reimbursements = new List<Reimbursement>();

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @" SELECT * FROM [ASCI_HR].[dbo].[ASCI_HFORM] where id_form = @id";

                result = conn.Query<HFormReim>(sql, new { id = id }).FirstOrDefault();

                if(result != null)
                {
                    sql = @" SELECT * FROM [ASCI_HR].[dbo].[ASCI_REIM] where id_form = @id";

                    result.details = conn.Query<Reimbursement>(sql, new { id = id }).ToList();
                }
            }
            return result;
        }
    }
}
