﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class BaseRepository : IDisposable
    {
        protected IConfiguration _config;
        protected string _connString;
        protected string _connKey;

        public BaseRepository(IConfiguration config)
        {
            _config = config;
            _connString = config.GetValue<String>("ConnectionStrings:APIDB");
        }

        public SqlConnection getConn()
        {
            return new SqlConnection(_connString);
        }

        public void Dispose()
        {

        }
    }
}
