﻿using ASCI_HRAPI.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class SettingRepository : BaseRepository, ISettingRepository
    {
        public SettingRepository(IConfiguration config) : base(config)
        {
        }

        public Setting getByName(string name)
        {
            Setting result = null;

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_SETTING] 
                                where 
                                    setting_name = @setting_name
                             ";

                result = conn.Query<Setting>(sql, new { setting_name = name }).FirstOrDefault();
            }

            return result;
        }

        public string getValueByName(string name)
        {
            return getValueByName(name, null);
        }

        public string getValueByName(string name, string defaultValue)
        {
            string result = defaultValue;

            Setting stg = getByName(name);

            if (stg != null)
                result = stg.setting_value;

            return result;
        }
    }
}
