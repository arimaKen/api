﻿using ASCI_HRAPI.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(IConfiguration config) : base(config)
        {

        }

        public User GetByUserName(string userName)
        {
            User result = null;

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_usr] 
                                where 
                                    username = @username
                             ";

                result = conn.Query<User>(sql, new { username = userName }).FirstOrDefault();
            }

            return result;
        }

        public User Get(string userName, string password)
        {
            User result = null;

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_usr] 
                                where 
                                    username = @username AND password = @passWord
                             ";

                result = conn.Query<User>(sql, new { username = userName, passWord = password }).FirstOrDefault();
            }

            return result;
        }
    }
}
