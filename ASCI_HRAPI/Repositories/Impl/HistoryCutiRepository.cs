﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class HistoryCutiRepository : BaseRepository, IHistoryCutiRepository
    {
        public HistoryCutiRepository(IConfiguration config) : base(config)
        {

        }
        public HistoryCuti getIdCuti(int id)
        {
            List<HistoryCuti> result = new List<HistoryCuti>();

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_HSTR_CUTI] 
                                where 
                                    id_cuti = @id
                             ";

                result = conn.Query<HistoryCuti>(sql, new { id = id }).ToList();
            }
            return null;
        }
    }
}
