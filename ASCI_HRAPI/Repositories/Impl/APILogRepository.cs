﻿using ASCI_HRAPI.Entities;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class APILogRepository : BaseRepository, IAPILogRepository
    {
        public APILogRepository(IConfiguration config) : base(config)
        {

        }

        public void addLog(APILog log)
        {
            using (var conn = getConn())
            {
                conn.Open();

                conn.Insert<APILog>(log);
            }
        }

        public void addLog(string url, string method, string ip_addr, string userName, string req, string status, string remark)
        {
            APILog log = new APILog();

            log.ip_addr = ip_addr;
            log.log_time = System.DateTime.Now;
            log.method = method;
            log.remark = remark;
            log.req_data = req;
            log.status = status;
            log.url = url;
            log.user_name = userName;

            addLog(log);
        }
    }
}
