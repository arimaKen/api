﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.Repositories;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class MasterCompanyRepository : BaseRepository, IMasterCompanyRepository
    {
        public MasterCompanyRepository(IConfiguration config) : base(config) { }

        public MasterCompany add(MasterCompany company)
        {
            MasterCompany result = null;
            using (var conn = getConn())
            {
                conn.Open();
                using (var tran = conn.BeginTransaction())
                {
                    MasterCompany data = new MasterCompany();

                    data.nama_company = company.nama_company;
                    data.alamat_company = company.alamat_company;
                    data.telpn = company.telpn;
                    data.email = company.email;
                    data.approved = "N";
                    conn.Insert<MasterCompany>(data, tran);
                    result = data;
                    tran.Commit();
                }
            }
            return result;

        }

        public MasterCompany getDetails(int id)
        {
            MasterCompany result = new MasterCompany();
            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"SELECT [id_company]
      ,[nama_company]
      ,[alamat_company]
      ,[telpn]
      ,[email]
  FROM [dbo].[ASCI_CMPNY] where [id_company] = @id";

                result = conn.Query<MasterCompany>(sql, new { id = id }).FirstOrDefault();
            }
            return result;
        }

        public List<MasterCompany> getMaster()
        {
            List<MasterCompany> result = new List<MasterCompany>();
            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"SELECT * from ASCI_CMPNY";

                result = conn.Query<MasterCompany>(sql).ToList();
            }
            return result;
        }
    }
}
