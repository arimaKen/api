﻿using System.Collections.Generic;
using System.Linq;
using ASCI_HRAPI.DTO;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class TypeReimbursementRepository : BaseRepository, ITypeReimbursementRepository
    {

        public TypeReimbursementRepository(IConfiguration config) : base(config) { }

    
        public List<TypeReimbursementDTO> GetTypeReimbursements()
        {
            List<TypeReimbursementDTO> result = null;

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @" SELECT *
  FROM [ASCI_HR].[dbo].[ASCI_TYPE_REIM]";

                result = conn.Query<TypeReimbursementDTO>(sql).ToList();
            }
            return result;
        }
    }
}
