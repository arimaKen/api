﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;
using MMS_API.Repositories.Exceptions;

namespace ASCI_HRAPI.Repositories.Impl
{
    public class EmployeeRepository : BaseRepository, IEmployeeRepository
    {
        public EmployeeRepository(IConfiguration config) : base(config)
        {
        }

        public Employee getByID(int id)
        {
            Employee result = null;

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_EMPL] 
                                where 
                                    id_employee = @id
                             ";

                result = conn.Query<Employee>(sql, new { id = id }).FirstOrDefault();

                if(result != null)
                {
                    List<Cuti> cuti = new List<Cuti>();

                    sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_CUTI] 
                                where 
                                    id_employee = @id
                             ";

                    cuti = conn.Query<Cuti>(sql, new { id = id }).ToList();

                    if(cuti != null)
                    {
                        result.emplCuti = cuti;
                        
                        List<HistoryCuti> hstrCuti = new List<HistoryCuti>();

                        sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_HSTR_CUTI] 
                                where 
                                    id_cuti = @id
                             ";

                        hstrCuti = conn.Query<HistoryCuti>(sql, new { id = id }).ToList();
                        if (hstrCuti != null)
                        {
                            result.emplCuti.ElementAt(0).hstrCuti = hstrCuti;
                        }
                    }
                }
            }

            return result;
        }

        public Employee login(string username, string password,string company)
        {
            Employee result = null;

            using (var conn = getConn())
            {
                conn.Open();

                string sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_EMPL] 
                                where 
                                    username = @username and company = @company
                             ";

                result = conn.Query<Employee>(sql, new { username = username, company = company }).FirstOrDefault();
                if (result != null)
                {
                         sql = @"
                                select 
                                    * 
                                from 
                                    [dbo].[ASCI_EMPL] 
                                where 
                                    username = @username and password = @password
                             ";

                        result = conn.Query<Employee>(sql, new { username = username, password = password }).FirstOrDefault();
                    if(result == null) throw new WrongPasswordException();


                }
                else
                {
                    throw new UserNotFoundException();
                }
            }
            
            return result;
        }
    }
}
