﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;

namespace ASCI_HRAPI.Repositories
{
    public interface IEmployeeRepository
    {
        Employee getByID(int id);
        Employee login(string username, string password, string company);
    }
}
