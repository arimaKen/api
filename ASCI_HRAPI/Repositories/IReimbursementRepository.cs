﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Entities;

namespace ASCI_HRAPI.Repositories
{
    public interface IReimbursementRepository
    {
        List<Reimbursement> getByIDEmployee(int id);
        Reimbursement getIdReim(string id);
        List<HFormReim> getForm(int id);
        HFormReim getFormDetail(int id);
        Reimbursement add(Reimbursement Reimbursment);
        List<TypeReimbursementDTO> GetTypeReimbursements();
        HFormReim addFrom(HFormReim hFormReim);

    }
}
