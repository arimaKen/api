﻿using ASCI_HRAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Repositories
{
    public interface IAPILogRepository
    {
        void addLog(APILog log);
        void addLog(string url, string method, string ip_addr, string userName, string req, string status, string remark);
    }
}
