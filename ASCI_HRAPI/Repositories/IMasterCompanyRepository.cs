﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.Entities;

namespace ASCI_HRAPI.Repositories
{
 public   interface IMasterCompanyRepository
    {
        MasterCompany getDetails(int id);
        MasterCompany add(MasterCompany company);
        List<MasterCompany> getMaster();

    }
}
