﻿using ASCI_HRAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Repositories
{
    public interface ISettingRepository
    {
        Setting getByName(string name);
        String getValueByName(string name);
        String getValueByName(string name, string defaultValue);
    }
}
