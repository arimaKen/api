﻿using ASCI_HRAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Repositories
{
    public interface IUserRepository
    {
        User GetByUserName(string userName);
        User Get(string userName, string password);
    }
}
