﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MMS_API.DTO
{
	public class APIResultLogin
	{
		public string status { get; set; }
		public string error { get; set; }
		public string message { get; set; }
		public UserDev data_status { get; set; }
		public string token { get; set; }
		public int retCode { get; set; }

		public class UserDev
		{
			public int usergroup_id { get; set; }
			public string username { get; set; }
			public string fullname { get; set; }
			public string user_active { get; set; }
			public string device_id { get; set; }
			
		}
	}
}