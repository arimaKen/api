﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.DTO
{
    public class AccessToken
    {
        public string name { get; set; }
        public string role { get; set; }
        public int exp { get; set; }
    }
}
