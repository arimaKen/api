﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.DTO
{
    public class APIResultHistoryCuti
    {
        public List<HistoryCutiDTO> Cuti { get; set; }

    }
}
