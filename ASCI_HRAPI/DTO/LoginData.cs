﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.DTO
{
    public class LoginData
    {
        public string username { set; get; }
        public string passwd { set; get; }
    }
}
