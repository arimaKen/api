﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.DTO
{
    public class FreedcampDTO
    {
        public string url { get; set; }
        public string user { get; set; }
        public string pass { get; set; }
        public string method { get; set; }

    }
}
