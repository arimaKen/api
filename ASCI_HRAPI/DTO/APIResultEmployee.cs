﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.DTO
{
    public class APIResultEmployee
    {
        public int idKaryawan { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string fullname { get; set; }
        public string position { get; set; }
        public string company { get; set; }

        [Computed]
        public List<CutiDTO> Cuti { get; set; }
    }
}
