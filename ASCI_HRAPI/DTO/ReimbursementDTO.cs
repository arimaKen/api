﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.DTO
{
    public class ReimbursementDTO
    {
        public int id_reim { get; set; }
        public String id_employee { get; set; }
        public int nominal { get; set; }
        public DateTime tanggal { get; set; }
        public string tipe { get; set; }
        public string catatan { get; set; }
        public string photo { get; set; }
    }
}
