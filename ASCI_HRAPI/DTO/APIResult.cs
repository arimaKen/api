﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.DTO
{
    public class APIResult
    {
        public string status { get; set; }
        public string error { get; set; }
        public string message { get; set; }
        public Object data_status { get; set; }
        public int retCode { get; set; }
        public string token { get; set; }
    }
}
