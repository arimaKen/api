﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;


namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_HSTR_CUTI")]
    public class HistoryCuti

    {
        [Key]
        public int id_History { get; set; }
        public int id_cuti { get; set; }
        public string employee_name { get; set; }
        public string type_cuti { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
        public string date_assign { get; set; }

        public string note { get; set; }
        public string lampiran { get; set; }
        public string verifikasi { get; set; }

    }
}
