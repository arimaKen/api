﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_REIM")]
    public class Reimbursement
    {
        [Key]
        public int id_reim { get; set; }
        public string id_form { get; set; }
        public string id_employee { get; set; }
        public int nominal { get; set; }
        public DateTime tanggal { get; set; }
        public string tipe { get; set; }
        public string catatan { get; set; }
        public string pathLocal { get; set; }
        public string photo { get; set; }
        public string status { get; set; }
    }
}
