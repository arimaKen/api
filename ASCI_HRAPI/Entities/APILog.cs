﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_APILOG")]
    public class APILog
    {
        [Key]
        public int id { get; set; }

        public string url { get; set; }
        public string method { get; set; }
        public string ip_addr { get; set; }
        public string user_name { get; set; }
        public DateTime? log_time { get; set; }
        public string req_data { get; set; }
        public string status { get; set; }
        public string remark { get; set; }
    }
}
