﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_SETTING")]
    public class Setting
    {
        [Key]
        public int setting_id { get; set; }

        [ExplicitKey]
        public string setting_name { get; set; }
        public string setting_value { get; set; }

        public string created_user { get; set; }
        public DateTime? created_date { get; set; }
        public string updated_user { get; set; }
        public DateTime? updated_date { get; set; }
    }
}
