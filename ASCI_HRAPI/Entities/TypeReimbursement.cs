﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_TYPE_REIM")]

    public class TypeReimbursement
    {
        public string type_reim { get; set; }
    }
}
