﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_USR")]
    public class User
    {
        [Key]
        public int user_id { get; set; }

        public int usergroup_id { get; set; }
        public string username { get; set; }
        public string fullname { get; set; }
        public string title { get; set; }
        public string password { get; set; }
        public string salt { get; set; }
        public string email { get; set; }
        public string user_active { get; set; }
        public string user_deleted { get; set; }
        public string last_ip { get; set; }
        public string api_key { get; set; }
        public string secret_api_key { get; set; }
        public DateTime? last_login { get; set; }
        public string created_user { get; set; }
        public DateTime? created_date { get; set; }
        public string updated_user { get; set; }
        public DateTime? updated_date { get; set; }

        [Computed]
        public string device_id { get; set; }
    }
}
