﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_CMPNY")]

    public class MasterCompany
    {
        [Key]
        public int id_company { get; set; }
        public string nama_company { get; set; }
        public string alamat_company { get; set; }
        public string telpn { get; set; }
        public string email { get; set; }
        public string approved { get; set; }


    }
}
