﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_DFORM")]
    public class DFormReim
    {
        [Key]
        public string id_reim_form { get; set; }
        public string id_form { get; set; }
        public string date { get; set; }
        public string tipe { get; set; }
        public string note { get; set; }
        public string nominal { get; set; }
    }
}
