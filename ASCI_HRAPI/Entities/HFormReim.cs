﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_HFORM")]
    public class HFormReim
    {
        [Key]
        public string id_form { get; set; }
        public string id_employee { get; set; }
        public string date_assign { get; set; }
        public string subject { get; set; }
        public string nominal { get; set; }
        public string status { get; set; }


        [Computed]
        public List<Reimbursement> details { get; set; }
    }
}
