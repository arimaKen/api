﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_EMPL")]
    public class Employee
    {
        [Key]
        public int id_employee { get; set; }

        public string nama_depan { get; set; }
        public string nama_belakang { get; set; }
        public string email { get; set; }
        public string posisi { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string company { get; set; }

        [Computed]
        public List<Cuti> emplCuti { get; set; }

    }
}
