﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ASCI_HRAPI.Entities
{
    [Table("ASCI_CUTI")]

    public class Cuti
    {
        [Key]
        public int id_cuti { get; set; }
        public int id_employee { get; set; }
        public string bulan { get; set; }
        public int jatah { get; set; }
        public int sisa { get; set; }


        [Computed]
        public List<HistoryCuti> hstrCuti { get; set; }

    }
}
