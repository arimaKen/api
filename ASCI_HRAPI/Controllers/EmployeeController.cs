﻿using System;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Services;
using Microsoft.AspNetCore.Mvc;
using MMS_API.Repositories.Exceptions;
using MMS_API.Services.Exceptions;
using Newtonsoft.Json;

namespace ASCI_HRAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class EmployeeController : Controller
    {
        protected ITokenService _tokenService;
        protected IEmployeeService _employeeService;
        protected IAPILogRepository _apilogRepository;
        protected ISettingRepository _settingService;

        public EmployeeController(ITokenService tokenService,IEmployeeService employeeService, IAPILogRepository apiLogRepository, ISettingRepository settingRepository)
        {
            _tokenService = tokenService;
            _employeeService = employeeService;
            _apilogRepository = apiLogRepository;
            _settingService = settingRepository;
        }

        [HttpGet("employee")]
        public IActionResult getbyid([FromQuery]string getbyid)
        {
            int retCode = 200;
            APIResult result = new APIResult(); 
            APIResultEmployee resEmpl = new APIResultEmployee();

            try
            {
                string userName = "";
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;
                resEmpl = _employeeService.getByID(Convert.ToInt32(getbyid));
                if (resEmpl == null)
                {
                    throw new MMS_API.Repositories.Exceptions.DataNotExistException();
                }
                else
                {
                    result.status = "Succedd";
                    result.message = "Employee found";
                    result.error = "";
                    result.data_status = resEmpl;
                    result.token = "";
                    result.retCode = 200;
                }
            }
            catch (MMS_API.Repositories.Exceptions.DataNotExistException)
            {
                result.error = "Employee not found";
                retCode = 401;
            }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, getbyid, JsonConvert.SerializeObject(getbyid, Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }

        [HttpGet("login")]
        public IActionResult login([FromQuery]string username, string password,string company)
        {
            {
                int retCode = 200;
                APIResult result = new APIResult();
                APIResultEmployee resEmpl = new APIResultEmployee();

                try
                {
                    string userName = "";
                    AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                    userName = atoken.name;

                    resEmpl = _employeeService.login(username, password, company);
                    if (resEmpl == null)
                    {
                        throw new MMS_API.Repositories.Exceptions.DataNotExistException();
                    }
                    else
                    {
                        result.status = "Succedd";
                        result.message = "Login Succedd";
                        result.error = "";
                        result.data_status = resEmpl;
                        result.token = "";
                        result.retCode = 200;
                    }
                }
                catch (MMS_API.Repositories.Exceptions.DataNotExistException)
                {
                    result.error = "Employee not found";
                    retCode = 401;
                }
                catch (UserNotFoundException)
                {
                    result.error = "User not found";
                    retCode = 401;
                }
                catch (WrongPasswordException)
                {
                    result.error = "Wrong password";
                    retCode = 401;
                }
                catch(TokenUnauthorizeException)
                {
                    result.error = "Invalid Token";
                    retCode = 401;
                }
                catch (TokenExpiredException)
                {
                    result.error = "Invalid Token";
                    retCode = 401;
                }
                catch (Exception e)
                {
                    result.error = "Invalid Parameter : " + e.Message;
                    retCode = 500;
                }

                _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, username, JsonConvert.SerializeObject(username, Formatting.Indented), retCode.ToString(), result.error);

                return new ObjectResultCode(retCode, result);
            }
        }
    }
}
