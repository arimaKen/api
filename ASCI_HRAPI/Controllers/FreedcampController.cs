﻿using System;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Services;
using Microsoft.AspNetCore.Mvc;
using MMS_API.Services.Exceptions;
using Newtonsoft.Json;

namespace ASCI_HRAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Freedcamp")]
    [Route("api/[controller]/[action]")]
    public class FreedcampController : Controller
    {
        protected ITokenService _tokenService;
        protected ISettingRepository _settingService;
        protected IAPILogRepository _apilogRepository;
        protected IFreedcampService _freedcampService;

        public FreedcampController(ITokenService tokenService, IAPILogRepository apilogRepository, ISettingRepository settingService, IFreedcampService freedcampService)
        {
            _tokenService = tokenService;
            _apilogRepository = apilogRepository;
            _settingService = settingService;
            _freedcampService = freedcampService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]FreedcampDTO dto)
        {
            string userName = "";
            int retCode = 200;
            APIResult result = new APIResult();

            try
            {
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;

                if (!string.IsNullOrEmpty(dto.method)) {
                    switch (dto.method) {
                        case "GET":
                            result = await _freedcampService.get(dto);
                            break;
                        default:
                            result.error = "Error";
                            break;
                    }
                }
                else
                {
                    result.error = "method cannot empty";
                }
                

                if ((result.error != null) && (!result.error.Equals("")))
                    retCode = 422;
            }
            catch (TokenExpiredException)
            {
                retCode = 401;
                result.error = "Token Expired";
            }
            catch (TokenUnauthorizeException)
            {
                retCode = 401;
                result.error = "Invalid Token";
            }
            catch (ValidationException e)
            {
                retCode = 422;
                result.error = e.Message;
            }
            catch (Exception e)
            {
                retCode = 500;
                result.error = "Invalid Parameter : " + e.Message;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, userName, JsonConvert.SerializeObject(dto, Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }
    }
}