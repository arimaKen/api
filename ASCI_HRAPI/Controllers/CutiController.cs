﻿using System;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Services;
using Microsoft.AspNetCore.Mvc;
using MMS_API.Services.Exceptions;
using Newtonsoft.Json;

namespace ASCI_HRAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/cuti")]
    public class CutiController : Controller

    {
        protected ITokenService _tokenService;
        protected IHistoryCutiService _historyService;
        protected IAPILogRepository _apilogRepository;
        protected ISettingRepository _settingService;

        public CutiController(ITokenService tokenService,IHistoryCutiService historyService, IAPILogRepository apiLogRepository, ISettingRepository settingRepository)
        {
            _tokenService = tokenService;
            _historyService = historyService;
            _apilogRepository = apiLogRepository;
            _settingService = settingRepository;
        }

        [HttpGet("getHistory")]
        public IActionResult getHistory([FromQuery]string id)
        {
            int retCode = 200;
            APIResult result = new APIResult(); 
            APIResultHistoryCuti resHistory = new APIResultHistoryCuti();

            try
            {
                string userName = "";
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;
                resHistory = _historyService.getIdCuti(Convert.ToInt32(id));
                if (resHistory == null) throw new DataNotExistException();
                result.status = "Succedd";
                result.message = "Employee found";
                result.error = "";
                result.data_status = JsonConvert.SerializeObject(resHistory);
                result.token = "";
                result.retCode = 200;
            }
            catch (DataNotExistException)
            {
                result.error = "Employee not found";
                retCode = 401;
            }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, id, JsonConvert.SerializeObject(id, Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }
    }
}
