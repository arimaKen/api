﻿using System;
using System.Collections.Generic;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Services;
using Microsoft.AspNetCore.Mvc;
using MMS_API.Repositories.Exceptions;
using MMS_API.Services.Exceptions;
using Newtonsoft.Json;

namespace ASCI_HRAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class MasterCompanyController : Controller
    {
        protected ITokenService _tokenService;
        protected IMasterCompanyService _masterCompany;
        protected IAPILogRepository _apilogRepository;
        protected ISettingRepository _settingService;


        public MasterCompanyController(ITokenService tokenService, IMasterCompanyService masterCompany, IAPILogRepository apiLogRepository, ISettingRepository settingRepository)
        {
            _tokenService = tokenService;
            _masterCompany = masterCompany;
            _apilogRepository = apiLogRepository;
            _settingService = settingRepository;
        }
        [HttpGet("company")]
        public IActionResult getdetails(int getdetails)
        {
            int retCode = 200;
            APIResult result = new APIResult();
            MasterCompany resComp = new MasterCompany();

            try
            {
                string userName = "";
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;

                resComp = _masterCompany.getDetail(getdetails);
                if (resComp == null)
                {
                    throw new MMS_API.Services.Exceptions.DataNotExistException();
                }
                else
                {
                    result.status = "Succedd";
                    result.message = "Company found";
                    result.error = "";
                    result.data_status = resComp;
                    result.token = "";
                    result.retCode = 200;
                }
            }
            catch (MMS_API.Services.Exceptions.DataNotExistException)
            {
                result.error = "Company not found";
                retCode = 401;
            }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, "", JsonConvert.SerializeObject(Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }

        [HttpGet("company/getMaster")]
        public IActionResult getMaster()
        {
            string userName = "";
            int retCode = 200;
            APIResult result = new APIResult();
            List<MasterCompany> resComp = new List<MasterCompany>();

            try
            {
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;
                resComp = _masterCompany.getMaster();
                if (resComp.Count == 0)
                {
                    throw new MMS_API.Services.Exceptions.DataNotExistException();
                }
                else
                {
                    result.status = "Succedd";
                    result.message = "Company found";
                    result.error = "";
                    result.data_status = resComp;
                    result.token = "";
                    result.retCode = 200;
                }
            }
            catch (MMS_API.Services.Exceptions.DataNotExistException)
            {
                result.error = "Company not found";
                retCode = 401;
            }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, "", JsonConvert.SerializeObject(Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }

        [HttpPost("company/add")]
        public IActionResult addFrom([FromBody]MasterCompany company)

        {
            string sStatus = "";
            string sErrMsg = "";
            int retCode = 500;
            APIResult result = new APIResult();
            MasterCompany dataStatus = new MasterCompany();
            if (company != null)
            {
                retCode = 200;
                sStatus = "SUCCEED";
                sErrMsg = "";
                try
                {
                    string userName = "";
                    AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                    userName = atoken.name;
                    dataStatus = _masterCompany.add(company);
                }
                catch (DataExistsException e)
                {
                    sStatus = "SUCCEED";
                    sErrMsg = e.Message;
                    retCode = 200;
                }
                catch (TokenUnauthorizeException)
                {
                    sStatus = "FAILED";
                    sErrMsg = "Invalid Token";
                    retCode = 401;
                }
                catch (TokenExpiredException)
                {
                    sStatus = "FAILED";
                    sErrMsg = "Invalid Token";
                    retCode = 401;
                }
                catch (Exception e)
                {
                    sStatus = "FAILED";
                    sErrMsg = "Invalid Parameter : " + e.Message;
                    retCode = 500;
                }

                if (retCode == 200)
                {
                    sErrMsg = (sErrMsg == "") ? "Form Pendaftaran Terkirim\nMohon tunggu approval" : sErrMsg;

                    result = new APIResult { status = sStatus, message = sErrMsg, error = "", data_status = dataStatus, retCode = retCode };
                }
                else
                    result = new APIResult { status = sStatus, message = "Reimbursment sent Failed", error = sErrMsg, data_status = "", retCode = retCode };

                _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, company.ToString(), JsonConvert.SerializeObject(company, Formatting.Indented), retCode.ToString(), result.error);
            }
            return new ObjectResultCode(retCode, result);
        }
    }
}
