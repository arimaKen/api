﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MMS_API.Services.Exceptions;
using Newtonsoft.Json;

namespace ASCI_HRAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Token")]
    public class TokenController : Controller
    {
        protected ITokenService _tokenService;
        protected IAPILogRepository _apilogRepository;
        protected ISettingRepository _settingService;

        public TokenController(ITokenService tokenService, IAPILogRepository apilogRepository, ISettingRepository settingRepository)
        {
            _tokenService = tokenService;
            _apilogRepository = apilogRepository;
            _settingService = settingRepository;
        }

        // GET: api/values
        [HttpPost]
        public IActionResult Login([FromBody]LoginData dto)
        {
            int retCode = 200;
            APIResult result = new APIResult();

            try
            {

                string tokenResult = _tokenService.login(dto.username, dto.passwd);

                if (tokenResult != null)
                {
                    result.status = "Succedd";
                    result.error = "";
                    result.retCode = retCode;
                    result.data_status = "";
                    result.message = "Request Token Succeed";
                    result.token = tokenResult;
                }
                else
                {
                    result.error = "Invalid token";
                    retCode = 401;
                }
            }
            catch (TokenUserNotFoundException)
            {
                result.error = "User not found";
                retCode = 401;
            }
            catch (TokenWrongPasswordException)
            {
                result.error = "Wrong password";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, dto.username, JsonConvert.SerializeObject(dto, Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }
    }
}