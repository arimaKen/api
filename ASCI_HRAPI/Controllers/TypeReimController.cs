﻿using System;
using System.Collections.Generic;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Services;
using Microsoft.AspNetCore.Mvc;
using MMS_API.Services.Exceptions;
using Newtonsoft.Json;

namespace ASCI_HRAPI.Controllers
{
    [Produces("application/json")]
    //[Route("api/Reim")]
    public class TypeReimController : Controller

        {
        protected ITokenService _tokenService;
            protected ITypeReimbursementService _employeeService;
            protected IAPILogRepository _apilogRepository;
            protected ISettingRepository _settingService;

            public TypeReimController(ITokenService tokenService,ITypeReimbursementService employeeService, IAPILogRepository apiLogRepository, ISettingRepository settingRepository)
            {
            _tokenService = tokenService;
                _employeeService = employeeService;
                _apilogRepository = apiLogRepository;
                _settingService = settingRepository;
            }

            [HttpGet("getType")]
            public IActionResult getType()

            {
                int retCode = 200;
                APIResult result = new APIResult();
                List<TypeReimbursementDTO> resEmpl = new List<TypeReimbursementDTO>();

                try
                {
                string userName = "";
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;
                resEmpl = _employeeService.getType();
                    if (resEmpl.Count == 0)
                    {
                        throw new DataNotExistException();
                    }
                    else
                    {
                        result.status = "Succedd";
                        result.message = "Reimbursement found";
                        result.error = "";
                        result.data_status = resEmpl;
                        result.token = "";
                        result.retCode = 200;
                    }
                }
                catch (DataNotExistException)
                {
                    result.error = "Reimbursement not found";
                    retCode = 401;
                }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
                {
                    result.error = "Invalid Parameter : " + e.Message;
                    retCode = 500;
                }

                _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, "", JsonConvert.SerializeObject(Formatting.Indented), retCode.ToString(), result.error);

                return new ObjectResultCode(retCode, result);
            }
        }

}
