﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASCI_HRAPI.DTO;
using ASCI_HRAPI.Repositories;
using ASCI_HRAPI.Entities;
using ASCI_HRAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MMS_API.Repositories.Exceptions;
using MMS_API.Services.Exceptions;
using Newtonsoft.Json;

namespace ASCI_HRAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class ReimbursementController : Controller

    {
        protected ITokenService _tokenService;
        protected IRembursementService _reimService;
        protected IAPILogRepository _apilogRepository;
        protected ISettingRepository _settingService;

        public ReimbursementController(ITokenService tokenService,IRembursementService reimService, IAPILogRepository apiLogRepository, ISettingRepository settingRepository)
        {
            _tokenService = tokenService;
            _reimService = reimService;
            _apilogRepository = apiLogRepository;
            _settingService = settingRepository;
        }

        [HttpGet("reim")]
        public IActionResult getReim([FromQuery]string getbyid)

        {
            int retCode = 200;
            APIResult result = new APIResult(); 
            List<APIResultReimbursement> resEmpl = new List<APIResultReimbursement>();

            try
            {
                string userName = "";
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;

                resEmpl = _reimService.getByIDEmployee(Convert.ToInt32(getbyid));
                if (resEmpl.Count == 0)
                {
                    throw new MMS_API.Repositories.Exceptions.DataNotExistException();
                }
                else
                {
                    result.status = "Succedd";
                    result.message = "Reimbursement found";
                    result.error = "";
                    result.data_status = resEmpl;
                    result.token = "";
                    result.retCode = 200;
                }
            }
            catch (MMS_API.Repositories.Exceptions.DataNotExistException)
            {
                result.error = "Reimbursement not found";
                retCode = 401;
            }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, getbyid, JsonConvert.SerializeObject(getbyid, Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }

        [HttpPost("reim/add")]
        public IActionResult postReim([FromBody]Reimbursement reimDTO)
        {
            string sStatus = "";
            string sErrMsg = "";
            int retCode = 500;
            APIResult result = new APIResult();
            if (reimDTO.id_employee != null)
            {
                retCode = 200;
                sStatus = "SUCCEED";
                sErrMsg = "";
                try
                {
                    string userName = "";
                    AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                    userName = atoken.name;
                    _reimService.add(reimDTO);
                }
                catch (DataExistsException e)
                {
                    sStatus = "SUCCEED";
                    sErrMsg = e.Message;
                    retCode = 200;
                }
                catch (TokenUnauthorizeException)
                {
                    result.error = "Invalid Token";
                    retCode = 401;
                }
                catch (TokenExpiredException)
                {
                    result.error = "Invalid Token";
                    retCode = 401;
                }
                catch (Exception e)
                {
                    sStatus = "FAILED";
                    sErrMsg = "Invalid Parameter : " + e.Message;
                    retCode = 500;
                }

                if (retCode == 200)
                {
                    sErrMsg = (sErrMsg == "") ? "Reimbursment sent Succeed." : sErrMsg;

                    result = new APIResult { status = sStatus, message = sErrMsg, error = "", data_status = reimDTO, retCode = retCode };
                }
                else
                    result = new APIResult { status = sStatus, message = "Reimbursment sent Failed", error = sErrMsg, data_status = "", retCode = retCode };

                _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, reimDTO.id_employee, JsonConvert.SerializeObject(reimDTO, Formatting.Indented), retCode.ToString(), result.error);
            }
            return new ObjectResultCode(retCode, result);
        }
        [HttpPost("reim/update")]
        public IActionResult updateReim([FromBody]Reimbursement reimDTO)
        {
            {
                string sStatus = "";
                string sErrMsg = "";
                int retCode = 500;
                APIResult result = new APIResult();
                if (reimDTO.id_reim > 0)
                {
                    retCode = 200;
                    sStatus = "SUCCEED";
                    sErrMsg = "";
                    try
                    {
                        string userName = "";
                        AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                        userName = atoken.name;
                        _reimService.add(reimDTO);
                    }
                    catch (DataExistsException e)
                    {
                        sStatus = "SUCCEED";
                        sErrMsg = e.Message;
                        retCode = 200;
                    }
                    catch (TokenUnauthorizeException)
                    {
                        result.error = "Invalid Token";
                        retCode = 401;
                    }
                    catch (TokenExpiredException)
                    {
                        result.error = "Invalid Token";
                        retCode = 401;
                    }
                    catch (Exception e)
                    {
                        sStatus = "FAILED";
                        sErrMsg = "Invalid Parameter : " + e.Message;
                        retCode = 500;
                    }

                    if (retCode == 200)
                    {
                        sErrMsg = (sErrMsg == "") ? "Edit Reimbursement Succeed." : sErrMsg;

                        result = new APIResult { status = sStatus, message = sErrMsg, error = "", data_status = reimDTO, retCode = retCode };
                    }
                    else
                        result = new APIResult { status = sStatus, message = "Reimbursment sent Failed", error = sErrMsg, data_status = "", retCode = retCode };

                    _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, reimDTO.id_employee, JsonConvert.SerializeObject(reimDTO, Formatting.Indented), retCode.ToString(), result.error);
                }
                else
                {
                    result = new APIResult { status = sStatus, message = "Reimbursment sent Failed", error = sErrMsg, data_status = "", retCode = retCode };
                }
                return new ObjectResultCode(retCode, result);
            }
        }

        [HttpGet("reim/getType")]
        public IActionResult getType()

        {
            int retCode = 200;
            APIResult result = new APIResult();
            List<TypeReimbursementDTO> resEmpl = new List<TypeReimbursementDTO>();

            try
            {
                string userName = "";
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;
                resEmpl = _reimService.getType();
                if (resEmpl.Count == 0)
                {
                    throw new MMS_API.Services.Exceptions.DataNotExistException();
                }
                else
                {
                    result.status = "Succedd";
                    result.message = "Reimbursement found";
                    result.error = "";
                    result.data_status = resEmpl;
                    result.token = "";
                    result.retCode = 200;
                }
            }
            catch (MMS_API.Services.Exceptions.DataNotExistException)
            {
                result.error = "Reimbursement not found";
                retCode = 401;
            }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, "", JsonConvert.SerializeObject(Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }

        [HttpGet("reim/getForm")]
        public IActionResult getForm([FromQuery]int getbyEmployee)

        {
            int retCode = 200;
            APIResult result = new APIResult();
            List<HFormReim> hFormReims = new List<HFormReim>();

            try
            {
                string userName = "";
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;

                hFormReims = _reimService.getForm(getbyEmployee);
                if (hFormReims.Count == 0)
                {
                    throw new MMS_API.Repositories.Exceptions.DataNotExistException();
                }
                else
                {
                    result.data_status = hFormReims;
                }
            }
            catch (MMS_API.Repositories.Exceptions.DataNotExistException)
            {
                result.error = "Reimbursement not found";
                retCode = 401;
            }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, getbyEmployee.ToString(), JsonConvert.SerializeObject(getbyEmployee, Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }


        [HttpGet("reim/getFormDetil")]
        public IActionResult getFormDetil([FromQuery]int getByIdForm)

        {
            int retCode = 200;
            APIResult result = new APIResult();
            HFormReim hFormReim = new HFormReim();

            try
            {
                string userName = "";
                AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                userName = atoken.name;

                hFormReim = _reimService.getFormDetil(getByIdForm);
                if (hFormReim == null)
                {
                    throw new MMS_API.Repositories.Exceptions.DataNotExistException();
                }
                else
                {
                    result.message = "detail found";
                    result.data_status = hFormReim;
                }
            }
            catch (MMS_API.Repositories.Exceptions.DataNotExistException)
            {
                result.error = "Reimbursement not found";
                retCode = 401;
            }
            catch (TokenUnauthorizeException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (TokenExpiredException)
            {
                result.error = "Invalid Token";
                retCode = 401;
            }
            catch (Exception e)
            {
                result.error = "Invalid Parameter : " + e.Message;
                retCode = 500;
            }

            _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, getByIdForm.ToString(), JsonConvert.SerializeObject(getByIdForm, Formatting.Indented), retCode.ToString(), result.error);

            return new ObjectResultCode(retCode, result);
        }


        [HttpPost("reim/addForm")]
        public IActionResult addFrom([FromBody]HFormReim hFormReim)
        {
            string sStatus = "";
            string sErrMsg = "";
            int retCode = 500;
            APIResult result = new APIResult();
            HFormReim dataStatus = new HFormReim();
            if (hFormReim != null)
            {
                retCode = 200;
                sStatus = "SUCCEED";
                sErrMsg = "";
                try
                {
                    string userName = "";
                    AccessToken atoken = _tokenService.validate(Request.Headers["Authorization"]);
                    userName = atoken.name;
                    if (hFormReim != null)
                    {
                     dataStatus =  _reimService.addForm(hFormReim);
                    }
                    else throw new Exception();
                }
                catch (DataExistsException e)
                {
                    sStatus = "SUCCEED";
                    sErrMsg = e.Message;
                    retCode = 200;
                }
                catch (TokenUnauthorizeException)
                {
                    sStatus = "FAILED";
                    sErrMsg = "Invalid Token";
                    retCode = 401;
                }
                catch (TokenExpiredException)
                {
                    sStatus = "FAILED";
                    sErrMsg = "Invalid Token";
                    retCode = 401;
                }
                catch (Exception e)
                {
                    sStatus = "FAILED";
                    sErrMsg = "Invalid Parameter : " + e.Message;
                    retCode = 500;
                }

                if (retCode == 200)
                {
                    sErrMsg = (sErrMsg == "") ? "Reimbursement Terkirim\nMohon tunggu approval" : sErrMsg;

                    result = new APIResult { status = sStatus, message = sErrMsg, error = "", data_status = dataStatus, retCode = retCode };
                }
                else
                    result = new APIResult { status = sStatus, message = "Reimbursment sent Failed", error = sErrMsg, data_status = "", retCode = retCode };

                _apilogRepository.addLog(Request.Path.Value, Request.Method, Request.Host.Host, hFormReim.id_employee, JsonConvert.SerializeObject(hFormReim, Formatting.Indented), retCode.ToString(), result.error);
            }
            return new ObjectResultCode(retCode, result);
        }
    }
}
