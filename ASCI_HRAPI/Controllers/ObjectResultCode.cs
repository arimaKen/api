﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ASCI_HRAPI.Controllers
{
    public class ObjectResultCode : ObjectResult
    {
        public ObjectResultCode(int code, object obj) : base(obj)
        {
            StatusCode = code;
        }
    }
}